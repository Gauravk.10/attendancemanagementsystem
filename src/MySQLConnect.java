
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;


public class MySQLConnect {
    public static Connection getConnection(){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/attendancedb", "gaurav", "gaurav");
        

            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"Connection Failed"+e);
            return null;
            
        }
    }
    public static void main(String args[]){
        Connection conn = MySQLConnect.getConnection();
        if(conn!=null)
            JOptionPane.showMessageDialog(null, "Connection Established");
            
    }
}
