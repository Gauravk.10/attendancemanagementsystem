
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gaura
 */
public class Defaulter {
    
    
    Connection conn;
    PreparedStatement ps;
    ResultSet rs;
    Defaulter(){
        conn = MySQLConnect.getConnection();
    }
    
    void getDefaulter(){
        System.out.println("inside defaulter");
        int count=0;
        int subject_counter;
        String query ="SELECT parent_email , subject_name , subject_counter from attendance JOIN student on student.student_id = attendance.student_id JOIN subject on subject.subject_id = attendance.subject_id where percentage < 75 ";
        try {
            ps = conn.prepareStatement(query);
         
            rs = ps.executeQuery();
            while(rs.next()){
                  System.out.println(rs.getInt("subject_counter") );
                if(rs.getInt("subject_counter") % 10 == 0){
                      
                    Mail.sendMail(rs.getString("parent_email"),rs.getString("subject_name"));
                    count++;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Defaulter.class.getName()).log(Level.SEVERE, null, ex);
        }
        query ="insert into defaulter(total) values(?)";
        try {
            ps = conn.prepareStatement(query);
            ps.setInt(1, count);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Defaulter.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
}
